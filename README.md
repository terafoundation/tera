﻿# TERA PLATFORM


We moved all the code to a new repository, as this one has grown very much (due to improper use of settings) and has become inconvenient to use.

Current project repository:
https://gitlab.com/terafoundation/tera2
